# mqttwatch

Script watches over MQTT topic and resets rtl_433.

## Deploy daily cron file

```
cp cron.daily/rtl_433 /etc/cron.daily/rtl_433
chmod 755 /etc/cron.daily/rtl_433
```

## Deploy sudoers file for cron script

```
cp sudoers.d/rtl_433 /etc/sudoers.d/rtl_433
chmod 400 /etc/sudoers.d/rtl_433
```

## Source

- [Python: Subscribing to MQTT topic](https://techtutorialsx.com/2017/04/23/python-subscribing-to-mqtt-topic/)
