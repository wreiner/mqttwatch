#!/usr/bin/python3

import paho.mqtt.client as mqtt
import time
import os

Connected = False
last_message = time.time()


def on_disconnect(client, userdata, rc):
    print("disconnecting reason  " + str(rc))
    client.connected_flag = False
    client.disconnect_flag = True


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("connected.")
        global Connected
        Connected = True
    else:
        sys.exit(1)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global last_message
    last_message = time.time()
    print("Message received-> " + msg.topic + " " +
          str(msg.payload))  # Print a received msg


# Create instance of client with client ID “digi_mqtt_test”
client = mqtt.Client("mqttwatch")

# Define callback function for successful connection/disconnect
client.on_connect = on_connect
client.on_disconnect = on_disconnect

# Define callback function for receipt of a message
client.on_message = on_message

client.connect('192.168.2.2', 1883)

print("starting loop ..")
client.loop_start()
# Wait for connection
while Connected != True:
    print(Connected)
    time.sleep(0.1)

print("will subscribe..")
client.subscribe("rtl_433/piholebu/events")

print("entering loop ..")
while True:
    if time.time() - last_message > 60:
        print("stalled, resetting rtl_433 ..")
        os.system('sudo /etc/cron.daily/rtl_433')
        last_message = time.time()
    time.sleep(6)
